from os import environ, path

from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

FLASK_APP = environ.get("FLASK_APP")
FLASK_ENV = environ.get("FLASK_ENV")
SECRET_KEY = environ.get("SECRET_KEY")

GOOGLE_API_KEY = environ.get("GOOGLE_API_KEY")

if not SECRET_KEY:
    raise ValueError("No SECRET_KEY set for Flask application")

if not GOOGLE_API_KEY:
    raise ValueError("No GOOGLE_API_KEY set for Flask application")
