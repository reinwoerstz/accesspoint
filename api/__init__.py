import os

from flask import Flask
from flask_cors import CORS
from flask_marshmallow import Marshmallow

ma = Marshmallow()


def create_app(test_config: dict = None) -> Flask:
    # create and configure the app
    app = Flask(__name__)
    CORS(app)
    ma.init_app(app)

    if test_config:
        app.config.from_mapping(test_config)
    else:
        app.config.from_pyfile("../config.py")

    from api import geolocation

    app.register_blueprint(geolocation.bp)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    return app
