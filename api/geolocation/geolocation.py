from flask import Blueprint
from flask import current_app as app
from flask import request
from marshmallow import ValidationError
from requests import HTTPError

from api.geolocation.schemas import AccessPointSchema
from api.geolocation.services import GeolocationService

bp = Blueprint("geolocate_bp", __name__, url_prefix="/geolocate")


@bp.route("/", methods=("POST",))
def geolocate_view():
    service = GeolocationService(app.config["GOOGLE_API_KEY"])

    if json := request.json:
        data = json.get("apscan_data", [])
    else:
        data = []

    try:
        ap_data = AccessPointSchema(many=True).load(data)
        return service.geolocate_for_access_point_data(ap_data)
    except ValidationError as error:
        return error.messages, 400
    except HTTPError as error:
        return str(error), error.response.status_code
