from datetime import datetime
from typing import List

import requests


class GeolocationService:
    def __init__(self, api_key):
        self.base_url = (
            f"https://www.googleapis.com/geolocation/v1/geolocate?key={api_key}"
        )

    def geolocate_for_access_point_data(self, ap_data: List[dict]) -> dict:
        """
        Returns geolocation data for the give access point data
        :param ap_data The access point data
        :raise HttpError
        :return
        """

        def map_data(data):
            return {
                "macAddress": data["bssid"],
                "strength": data["rssi"],
                "age": round(datetime.now().timestamp()) - data["timestamp"],
                "channel": data["channel"],
            }

        mapped_data = list(map(map_data, ap_data))

        response = requests.post(
            self.base_url, json={"wifiAccessPoints": mapped_data}, timeout=15
        )

        if response.ok:
            return response.json()
        else:
            response.raise_for_status()
